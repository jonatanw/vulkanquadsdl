# VulkanQuadSDL - single file minimal Vulkan example

This is a fairly extensive modification of the first triangle code of the excellent vulkan-tutorial.com tutorial [1] as a stand-alone app.

Besides minor cleanups, reorganizations, improvements, and stylistic changes, the main differences are

- Compile shaders at runtime. While I agree that compile-time SPIRV shader creation is generally the better option, sometimes you need to have dynamic runtime shader compilation, which this demonstrates how to do. Plus, as a simple to build single-file example, this avoids separate shader files and complicated build and deploy steps.

- Switch from glfw to SDL - since most Vulkan examples use glfw I wanted to have an SDL variant as well.

[1] Specifically, https://github.com/Overv/VulkanTutorial/blob/main/code/19_vertex_buffer.cpp
